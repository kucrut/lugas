const node_to_string = require( 'hast-util-to-string' );
const refractor = require( 'refractor' );
const rehype = require( 'rehype' );
const visit = require( 'unist-util-visit' );

function get_language( node, parent ) {
	const className = node.properties.className || [];

	for ( const classListItem of className ) {
		if ( classListItem.slice( 0, 9 ) === 'language-' ) {
			return classListItem.slice( 9 ).toLowerCase();
		}
	}

	if ( parent ) {
		return get_language( parent );
	}

	return null;
}

function dz_prism( options = {} ) {
	function visitor( node, index, parent ) {
		if ( ! parent || parent.tagName !== 'pre' || node.tagName !== 'code' ) {
			return;
		}

		const lang = get_language( node, parent );

		if ( lang === null ) {
			return;
		}

		let result;

		try {
			result = refractor.highlight( node_to_string( node ), lang );
		} catch ( err ) {
			if ( options.ignoreMissing && /Unknown language/.test( err.message ) ) {
				return;
			}
			throw err;
		}

		node.children = result;
		node.properties.tabIndex = '0';
	}

	return tree => {
		visit( tree, 'element', visitor );
	};
}

function get_processor( options ) {
	const processor = rehype().data( 'settings', { fragment: true } ).use( dz_prism, options );

	return processor;
}

module.exports = { get_processor };
