# Prism

Based on https://github.com/mapbox/rehype-prism.

## Usage

1. [Activate](https://elderguide.com/tech/elderjs/#plugins) this plugin via `elder.config.js`.
1. Import the stylesheets you desire:

```css
@import 'prismjs/themes/prism-coy.css' ( prefers-color-scheme: light );
@import 'prismjs/themes/prism-okaidia.css' ( prefers-color-scheme: dark );
```
