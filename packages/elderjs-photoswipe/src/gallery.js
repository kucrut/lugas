function createGalleryScript( path ) {
	return `( async () => {
const gallery_els = document.querySelectorAll( '.gallery' );

if ( ! gallery_els.length ) {
	return;
}

try {
	const { init_ps } = await import( '${ path }' );
	init_ps( gallery_els );
} catch ( error ) {
	// eslint-disable-next-line no-console
	console.error( error );
}
} )();`;
}

module.exports = createGalleryScript;
