import PhotoSwipe from 'photoswipe/dist/photoswipe.js';
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default.js';

const collection = [];
const item_class = '.gallery-item';
const search_token = '/f_auto,q_auto';
let instance;
let pswp_root;

function inject_markup() {
	pswp_root = document.createElement( 'div' );

	pswp_root.id = 'pswp';
	pswp_root.className = 'pswp';
	pswp_root.setAttribute( 'role', 'dialog' );
	pswp_root.setAttribute( 'aria-hidden', 'true' );
	pswp_root.setAttribute( 'tabindex', '-1' );
	pswp_root.innerHTML = `
<div class="pswp__bg"></div>

<div class="pswp__scroll-wrap">
	<div class="pswp__container">
		<div class="pswp__item"></div>
		<div class="pswp__item"></div>
		<div class="pswp__item"></div>
	</div>

	<div class="pswp__ui pswp__ui--hidden">
		<div class="pswp__top-bar">
			<div class="pswp__counter"></div>

			<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
			<button class="pswp__button pswp__button--share" title="Share"></button>
			<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
			<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

			<div class="pswp__preloader">
				<div class="pswp__preloader__icn">
					<div class="pswp__preloader__cut">
					<div class="pswp__preloader__donut"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
			<div class="pswp__share-tooltip"></div>
		</div>

		<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
		</button>

		<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
		</button>

		<div class="pswp__caption">
			<div class="pswp__caption__center"></div>
		</div>
	</div>
</div>
`;

	document.body.appendChild( pswp_root );
}

function set_item_props( item, props ) {
	item.h = props.h;
	item.w = props.w;
	item.src = props.src;
}

function set_item_data( idx, item ) {
	const width = instance.viewportSize.x;

	// It's already set and the viewport didn't change.
	if ( item.w === width ) {
		return;
	}

	const img_w = Math.ceil( window.devicePixelRatio * width );
	const { fsrc, mwidth, mheight, sizes } = item;
	let props = sizes.find( s => s.img_w === img_w );

	// Already set once before.
	if ( props ) {
		set_item_props( item, props );
		return;
	}

	props = {
		img_w,
		w: width,
		h: ( width / mwidth ) * mheight,
		src: fsrc.replace( search_token, `${ search_token }/w_${ img_w },c_scale` ),
	};

	item.sizes.push( props );
	set_item_props( item, props );
}

function handle_click( id, e ) {
	const item_el = e.target.closest( item_class );

	if ( ! item_el ) {
		return;
	}

	e.preventDefault();
	const gallery = collection.find( i => i.id === id );

	if ( ! gallery ) {
		return;
	}

	const index = Array.from( gallery.item_els ).indexOf( item_el );
	instance = new PhotoSwipe( pswp_root, PhotoSwipeUI_Default, gallery.items, { index, history: false } );

	instance.listen( 'gettingData', set_item_data );
	instance.init();
}

function generate_items( items ) {
	const result = [];

	items.forEach( i => {
		const img = i.querySelector( 'img' );
		const fsrc = img.parentElement.getAttribute( 'href' );
		const msrc = img.src;
		const mheight = parseInt( img.getAttribute( 'height' ), 10 );
		const mwidth = parseInt( img.getAttribute( 'width' ), 10 );

		result.push( { fsrc, mheight, msrc, mwidth, sizes: [] } );
	} );

	return result;
}

function create_instance( gallery_el ) {
	const item_els = gallery_el.querySelectorAll( item_class );

	if ( ! item_els.length ) {
		return;
	}

	const items = generate_items( item_els );
	const visible = Array.from( item_els ).filter( i => getComputedStyle( i ).display !== 'none' );

	if ( visible.length < items.length ) {
		const last_visible = visible[ visible.length - 1 ];
		const button = last_visible.querySelector( 'a' );

		last_visible.classList.add( 'with-cover' );

		button.dataset.hiddenCount = item_els.length - visible.length;
	}

	collection.push( {
		item_els,
		items,
		id: gallery_el.id,
	} );

	gallery_el.addEventListener( 'click', ( ...args ) => handle_click( gallery_el.id, ...args ) );
}

export async function init_ps( gallery_els ) {
	inject_markup();
	gallery_els.forEach( create_instance );
}
