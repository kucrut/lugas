const flat_cache = require( 'flat-cache' );
const node_fetch = require( 'node-fetch' );
const os_path = require( 'path' );
const { URL, URLSearchParams } = require( 'url' );

function parse_response( response ) {
	return response.json();
}

async function fetch( url, options = {} ) {
	const { params = null, parser = parse_response, cache = null, ...rest } = options;
	const url_object = new URL( url );
	let cache_key;

	if ( typeof params === 'object' ) {
		url_object.search = new URLSearchParams( params ).toString();
	}

	if ( cache ) {
		cache_key = url_object.toString();
		const cached_data = cache.getKey( cache_key );

		if ( cached_data ) {
			return cached_data;
		}
	}

	const result = await node_fetch( url_object, rest ).then( parser );

	if ( cache ) {
		cache.setKey( cache_key, result );
		cache.save( true );
	}

	return result;
}

function dzwp_fetch_parser( response ) {
	const result = {};

	return response.json().then( data => {
		if ( response.ok ) {
			result.data = data;

			if ( Array.isArray( data ) ) {
				result.headers = {
					total_items: parseInt( response.headers.get( 'x-wp-total' ), 10 ) || -1,
					total_pages: parseInt( response.headers.get( 'x-wp-totalpages' ), 10 ) || -1,
				};
			}

			return result;
		}

		const err = new Error( data.message || 'Unknown server error' );
		err.code = data.code || '__unknown';
		err.response = response;
		err.data = data;
		throw err;
	} );
}

function dzwp_make_fetch( api_url, cache_group, parser = dzwp_fetch_parser ) {
	const cache = flat_cache.load( cache_group, os_path.join( process.cwd(), '.cache' ) );

	function wp_fetch( path, options = {} ) {
		return fetch( api_url + path, {
			cache,
			parser,
			...options,
		} );
	}

	return wp_fetch;
}

function dzwp_make_fetch_all( fetcher ) {
	async function wp_fetch_all( path, options = {} ) {
		const merged_options = {
			...options,
			params: {
				...options.params,
				per_page: 100,
			},
		};

		const result = await fetcher( path, merged_options );
		const total_pages = Number( result?.headers?.total_pages || -1 );

		if ( total_pages < 2 ) {
			return result;
		}

		const pages = [ ...Array( total_pages + 1 ).keys() ].slice( 2 );

		const fetch_group = async page => {
			const response = await fetcher( path, {
				...merged_options,
				params: {
					...merged_options.params,
					page,
				},
			} );

			return response;
		};

		const merge_all = groups => {
			groups.forEach( group => {
				result.data = [].concat( result.data, group.data );
			} );
		};

		await Promise.all( pages.map( fetch_group ) ).then( merge_all );

		return result;
	}

	return wp_fetch_all;
}

module.exports = { fetch, dzwp_fetch_parser, dzwp_make_fetch, dzwp_make_fetch_all };
