const hooks = [
	{
		hook: 'bootstrap',
		name: 'dzwp_add_data_helpers_settings',
		description: 'Add initial data, helpers & settings for WordPress-related functionalities.',
		priority: 99,
		run: async ( { data, helpers, plugin, settings } ) => {
			const api_url = plugin.config.url + '/wp-json';
			const dzwp_fetch = plugin.helpers.dzwp_make_fetch( api_url, 'dzwp' );
			const dzwp_fetch_all = plugin.helpers.dzwp_make_fetch_all( dzwp_fetch );
			const response = await dzwp_fetch( '/bridge/v1/info' );

			return {
				data: {
					...data,
					wp_info: response.data,
					wp_api_url: api_url,
				},
				helpers: {
					...helpers,
					...plugin.helpers,
					dzwp_fetch,
					dzwp_fetch_all,
				},
				settings: {
					...settings,
					dzwp_config: plugin.config,
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_menus',
		description: 'Fetch WordPress menus.',
		priority: 50,
		run: async ( { data, helpers, settings } ) => {
			async function fetch_menu( location ) {
				const response = await helpers.dzwp_fetch( `/bridge/v1/menus/${ location }` );

				return { location, menu: response.data };
			}

			function format_item( item ) {
				return helpers.dzwp_format_menu_item_data( {
					item,
					origin: settings.origin,
					wp_url: data.wp_info.url,
				} );
			}

			function format( { menu, ...rest } ) {
				return {
					...rest,
					menu: {
						...menu,
						items: menu.items.map( format_item ),
					},
				};
			}

			const menus = await Promise.all( data.wp_info.menu_locations.map( fetch_menu ) );

			return {
				data: {
					...data,
					menus: menus.map( format ),
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_users',
		description: 'Fetch WordPress users.',
		priority: 50,
		run: async ( { data, helpers, settings } ) => {
			function format( item ) {
				return helpers.dzwp_format_user_data( {
					item,
					origin: settings.origin,
					wp_url: data.wp_info.url,
				} );
			}

			const response = await helpers.dzwp_fetch_all( '/wp/v2/users/' );

			return {
				data: {
					...data,
					users: response.data.map( format ),
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_types',
		description: 'Fetch WordPress post types.',
		priority: 50,
		run: async ( { data, helpers } ) => {
			const response = await helpers.dzwp_fetch_all( '/wp/v2/types/' );

			return {
				data: {
					...data,
					types: Object.values( response.data ),
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_taxonomies',
		description: 'Fetch WordPress taxonomies.',
		priority: 50,
		run: async ( { data, helpers } ) => {
			const response = await helpers.dzwp_fetch_all( '/wp/v2/taxonomies/' );

			return {
				data: {
					...data,
					taxonomies: Object.values( response.data ),
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_terms',
		description: 'Fetch WordPress taxonomy terms.',
		priority: 50,
		run: async ( { data, helpers, settings } ) => {
			async function fetch_tax_terms( tax ) {
				const response = await helpers.dzwp_fetch_all( '/wp/v2/' + tax.rest_base );

				return response.data;
			}

			function format( item ) {
				return helpers.dzwp_format_term_data( {
					item,
					origin: settings.origin,
					wp_url: data.wp_info.url,
				} );
			}

			const terms = await Promise.all( data.taxonomies.map( fetch_tax_terms ) ).then( all => all.flat() );

			return {
				data: {
					...data,
					terms: terms.map( format ),
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'dzwp_fetch_pages_posts',
		description: 'Fetch WordPress pages & posts.',
		priority: 50,
		run: async ( { data, helpers, settings } ) => {
			function format( item ) {
				return helpers.dzwp_format_article_data( {
					item,
					config: settings.dzwp_config,
					origin: settings.origin,
					wp_url: data.wp_info.url,
					wp_entities: {
						taxonomies: data.taxonomies,
						terms: data.terms,
						types: data.types,
						users: data.users,
					},
				} );
			}

			const a_response = await helpers.dzwp_fetch_all( '/wp/v2/posts/' );
			const p_response = await helpers.dzwp_fetch_all( '/wp/v2/pages/' );

			return {
				data: {
					...data,
					articles: a_response.data.map( format ),
					pages: p_response.data.map( format ),
				},
			};
		},
	},
	{
		hook: 'stacks',
		name: 'dzwp_inject_html_attributes',
		description: 'Inject HTML attributes & such.',
		priority: 10,
		run: ( { data, htmlAttributesStack } ) => {
			const { wp_info } = data;
			const { html_dir, lang } = wp_info;

			return {
				htmlAttributesStack: [
					...htmlAttributesStack,
					{
						source: 'inject_html_attributes',
						string: `class="no-js" dir="${ html_dir }" lang="${ lang }"`,
						priority: 90,
					},
				],
			};
		},
	},
	{
		hook: 'head',
		name: 'dzwp_add_head_stuff',
		description: 'Add stuf to <head/>',
		priority: 10,
		run: ( { data, headString } ) => {
			const { wp_info } = data;
			const { home, name, site_icon_url } = wp_info;

			let newHeadString = `${ headString }
<link rel="alternate" type="application/rss+xml" title="${ name }" href="${ home }/feed" />`;

			if ( site_icon_url ) {
				newHeadString += `
<link rel="icon" href="${ site_icon_url }" />
<link rel="apple-touch-icon" href="${ site_icon_url }" />
<meta name="msapplication-TileImage" content="${ site_icon_url }" />`;
			}

			return {
				headString: newHeadString,
			};
		},
	},
];

module.exports = hooks;
