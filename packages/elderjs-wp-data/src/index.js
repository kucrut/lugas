const helpers = require( './helpers' );
const hooks = require( './hooks' );

const dzwp = {
	hooks,
	name: '@valiant.id/elderjs-wp-data',
	description: 'Get data from WP API.',
	config: {
		datetime_format: {
			date_style: 'medium',
			locale: 'en-US',
		},
		url: process.env.WP_DATA_URL,
	},
	init: plugin => ( {
		helpers,
		...{
			...plugin,
			config: {
				...plugin.config,
				url: plugin.config.url.replace( /\/*$/, '' ),
			},
		},
	} ),
};

module.exports = dzwp;
exports.default = dzwp;
