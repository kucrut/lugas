/**
 * Helpers
 */

const api_helpers = require( './api' );

// General

function dzwp_leadingslashit( str ) {
	return str.replace( /^[\/]{0,}/, '/' );
}

function dzwp_trailingslashit( str ) {
	return str.replace( /[\/]{0,}$/, '/' );
}

function dzwp_untrailingslashit( str ) {
	return str.replace( /[\/]{1,}$/, '' );
}

// URL

function dzwp_strip_origin( url, origin ) {
	const regex = new RegExp( `^${ origin }` );

	return url.replace( regex, '' );
}

// Data

function dzwp_format_article_data( arg ) {
	const { item, config, origin, wp_url, wp_entities } = arg;
	const { datetime_format } = config;
	const { type } = item;
	const { taxonomies, terms, types, users } = wp_entities;
	const post_type = types.find( i => i.slug === type );

	const date_object = new Date( Date.parse( item.date_gmt ) );
	const dzwp_date_gmt_display = new Intl.DateTimeFormat( datetime_format.locale, {
		dateStyle: datetime_format.date_style,
	} ).format( date_object );
	const dzwp_link = dzwp_trailingslashit( item.link.replace( wp_url, origin ) );

	const extras = {
		dzwp_date_gmt_display,
		dzwp_link,
		dzwp_permalink: dzwp_strip_origin( dzwp_link, origin ),
		dzwp_term_ids: {},
	};

	const author = users.find( user => user.id === item.author );

	if ( author ) {
		extras.dzwp_author = {
			avatar_urls: author.avatar_urls,
			dzwp_link: author.dzwp_link,
			name: author.name,
		};
	}

	if ( post_type.taxonomies.length ) {
		taxonomies.forEach( tax => {
			const { rest_base, slug } = tax;
			const { [ rest_base ]: ids = [] } = item;

			extras.dzwp_term_ids[ tax.slug ] = ids;
			extras[ rest_base ] = terms.filter( t => ids.includes( t.id ) && t.taxonomy === slug );
		} );
	}

	return { ...item, ...extras };
}

function dzwp_format_menu_item_data( arg ) {
	const { item, origin, wp_url } = arg;
	const fixed_url = item.url.replace( wp_url, origin );
	const url = fixed_url !== item.url ? dzwp_trailingslashit( fixed_url ) : item.url;

	return {
		...item,
		url,
	};
}

function dzwp_format_term_data( arg ) {
	const { item, origin, wp_url } = arg;
	const dzwp_link = dzwp_trailingslashit( item.link.replace( wp_url, origin ) );

	return {
		...item,
		dzwp_link,
		dzwp_permalink: dzwp_strip_origin( dzwp_link, origin ),
	};
}

function dzwp_format_user_data( arg ) {
	const { item, origin, wp_url } = arg;
	return {
		...item,
		dzwp_link: dzwp_trailingslashit( item.link.replace( wp_url, origin ) ),
	};
}

// Page

function dzwp_generate_doc_title(
	current_page = 1,
	site_name,
	site_desc = '',
	queried_object = null,
	separator = '–',
) {
	const paged = current_page > 1 ? ` ${ separator } Page ${ current_page }` : '';

	if ( queried_object === null ) {
		return site_desc ? `${ site_name }${ paged } ${ separator } ${ site_desc }` : `${ site_name }${ paged }`;
	}

	const prefix = queried_object?.name || queried_object?.title?.rendered;

	return `${ prefix }${ paged } ${ separator } ${ site_name }`;
}

// Routes

function dzwp_generate_paged_paths( current_page, total_pages, path = '' ) {
	const next_page = current_page < total_pages ? `${ path }/page/${ current_page + 1 }/` : null;
	let prev_page;

	if ( current_page > 1 ) {
		prev_page = current_page === 2 ? `${ path }/` : `${ path }/page/${ current_page - 1 }/`;
	} else {
		prev_page = null;
	}

	return { next_page, prev_page };
}

function dzwp_generate_home_routes( posts_count, per_page ) {
	const total_pages = Math.ceil( posts_count / per_page );
	const routes = [];

	for ( let i = total_pages; i > 0; i-- ) {
		routes.push( {
			...dzwp_generate_paged_paths( i, total_pages ),
			page: i,
			permalink: i === 1 ? '/' : `/page/${ i }/`,
			slug: null,
		} );
	}

	return routes;
}

function dzwp_generate_singular_routes( collection ) {
	return collection.map( item => ( {
		permalink: item.dzwp_permalink,
		slug: item.slug,
	} ) );
}

function dzwp_generate_single_term_routes( term, per_page ) {
	const permalink = dzwp_untrailingslashit( term.dzwp_permalink );
	const total_pages = Math.ceil( term.count / per_page );
	const term_routes = [];

	for ( let i = total_pages; i > 0; i-- ) {
		term_routes.push( {
			...dzwp_generate_paged_paths( i, total_pages, permalink ),
			page: i,
			permalink: i === 1 ? `${ permalink }/` : `${ permalink }/page/${ i }/`,
			slug: term.slug,
			taxonomy: term.taxonomy,
		} );
	}

	return term_routes;
}

function dzwp_generate_all_term_routes( taxonomies, terms, per_page ) {
	const routes = taxonomies
		.map( tax => {
			const tax_terms = terms.filter( term => term.taxonomy === tax.slug );
			const term_routes = tax_terms.map( term => dzwp_generate_single_term_routes( term, per_page ) ).flat();

			return term_routes;
		} )
		.flat();

	return routes;
}

function dzwp_get_posts_slice_indeces( current_page, per_page ) {
	const start = current_page === 1 ? 0 : ( current_page - 1 ) * per_page;
	const end = current_page * per_page;

	return [ start, end ];
}

function dzwp_get_home_route_posts( all_posts, current_page, per_page ) {
	const slice_indeces = dzwp_get_posts_slice_indeces( current_page, per_page );
	const posts = all_posts.slice( ...slice_indeces );

	return posts;
}

function dzwp_get_term_route_posts( term, all_posts, current_page, per_page ) {
	const slice_indeces = dzwp_get_posts_slice_indeces( current_page, per_page );
	const filtered = all_posts.filter( i => i.dzwp_term_ids[ term.taxonomy ].includes( term.id ) );
	const posts = filtered.slice( ...slice_indeces );

	return posts;
}

module.exports = {
	...api_helpers,
	dzwp_format_article_data,
	dzwp_format_menu_item_data,
	dzwp_format_term_data,
	dzwp_format_user_data,
	dzwp_generate_all_term_routes,
	dzwp_generate_doc_title,
	dzwp_generate_home_routes,
	dzwp_generate_paged_paths,
	dzwp_generate_single_term_routes,
	dzwp_generate_singular_routes,
	dzwp_get_home_route_posts,
	dzwp_get_term_route_posts,
	dzwp_leadingslashit,
	dzwp_strip_origin,
	dzwp_trailingslashit,
	dzwp_untrailingslashit,
};
