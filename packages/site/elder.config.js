require( 'dotenv' ).config();

const { NODE_ENV = 'production', ORIGIN } = process.env;

const plugins = {
	'@valiant.id/elderjs-photoswipe': {},
	'@valiant.id/elderjs-prism': {},
	'@valiant.id/elderjs-wp-data': {
		datetime_format: {
			date_style: 'medium',
			locale: 'id-ID',
		},
	},
};

if ( NODE_ENV === 'development' ) {
	plugins[ '@elderjs/plugin-browser-reload' ] = {
		// this reloads your browser when nodemon restarts your server.
		port: 8080,
		reload: false, // if you are having issues with reloading not working, change to true.
	};
}

module.exports = {
	plugins,
	build: {},
	css: 'file',
	debug: {
		automagic: false,
		build: false,
		hooks: false,
		performance: false,
		stacks: false,
	},
	distDir: 'public',
	hooks: {
		disable: [ 'elderAddDefaultIntersectionObserver' ],
	},
	lang: 'en',
	origin: ORIGIN,
	rootDir: process.cwd(),
	server: {
		prefix: '',
	},
	shortcodes: { closePattern: '}}', openPattern: '{{' },
	srcDir: 'src',
};
