module.exports = {
	all: ( { data, helpers } ) =>
		helpers.dzwp_generate_all_term_routes( data.taxonomies, data.terms, data.wp_info.settings.archive.per_page ),
	data: ( { data, helpers, request } ) => {
		const filter = { taxonomy: request.taxonomy, slug: request.slug };
		const queried_object = data.terms.find( i => Object.keys( filter ).every( key => i[ key ] === filter[ key ] ) );

		return {
			queried_object,
			articles: helpers.dzwp_get_term_route_posts(
				queried_object,
				data.articles,
				request.page,
				data.wp_info.settings.archive.per_page,
			),
			doc_title: helpers.dzwp_generate_doc_title(
				request.page,
				data.wp_info.name,
				data.wp_info.description,
				queried_object,
			),
		};
	},
	permalink: ( { request } ) => request.permalink,
};
