const path = require( 'path' );
const fs = require( 'fs-extra' );
const clean_css = require( 'clean-css' );

const { NODE_ENV = 'production', SERVER_PORT } = process.env;

/**
 * Hooks
 *
 * See https://elderguide.com/tech/elderjs/#hooks
 */

module.exports = [
	{
		hook: 'bootstrap',
		name: 'set_proper_origin',
		description: 'Set proper origin URL.',
		priority: 99,
		run: async ( { settings } ) => {
			const { origin } = settings;

			return {
				settings: {
					...settings,
					origin: NODE_ENV === 'development' ? `${ origin }:${ SERVER_PORT }` : origin,
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'copy_assets',
		description: 'Copy assets to public/_assets/ dir.',
		run: ( { settings } ) => {
			const src_dir = path.resolve( settings.rootDir, 'assets' );

			if ( ! fs.existsSync( src_dir ) ) {
				return;
			}

			const dest_dir = path.resolve( settings.distDir, '_assets' );

			fs.ensureDirSync( dest_dir );
			fs.copySync( src_dir, dest_dir );
		},
	},
	{
		hook: 'bootstrap',
		name: 'create_critical_css',
		description: 'Create critical CSS for all pages.',
		run: async ( { data, settings } ) => {
			const css_file = path.resolve( settings.rootDir, './src/styles/critical.css' );
			const content = fs.readFileSync( css_file, 'utf8' );
			let critical_css;

			if ( NODE_ENV === 'production' ) {
				const cleaner = new clean_css( { level: 1, sourceMap: false } );
				const minified = cleaner.minify( content );
				critical_css = minified.styles;
			} else {
				critical_css = content;
			}

			return {
				data: {
					...data,
					critical_css,
				},
			};
		},
	},
	{
		hook: 'stacks',
		name: 'inject_critical_css',
		description: 'Inject critical CSS to <head>.',
		run: ( { data, headStack } ) => {
			return {
				headStack: [
					...headStack,
					{
						source: 'inject_critical_css',
						string: `<style>${ data.critical_css }</style>`,
						priority: 90,
					},
				],
			};
		},
	},
	{
		hook: 'stacks',
		name: 'inject_critical_js',
		description: 'Inject critical JS to <head>.',
		run: ( { headStack } ) => {
			return {
				headStack: [
					...headStack,
					{
						source: 'inject_critical_js',
						string:
							"<script>(function(c){c.add('has-js');c.remove('no-js')})(document.documentElement.classList)</script>",
						priority: 90,
					},
				],
			};
		},
	},
];
