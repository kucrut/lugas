module.exports = {
	plugins: [
		require( 'postcss-import' ),
		require( 'postcss-custom-media' ),
		require( 'postcss-custom-properties' ),
		require( 'postcss-nested' ),
		require( 'postcss-url' )( { url: 'inline' } ),
		require( 'autoprefixer' ),
	],
	whitelistPatterns: [ /svelte-/ ],
};
