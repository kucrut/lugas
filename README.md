# Degil

Static frontend for WordPress sites built with [Elder.js](https://elderguide.com/tech/elderjs/).

## Disclaimer

Although I'm using this on [my personal site](https://dz.aziz.im), it is a hobby project that comes with NO WARRANTIES whatsoever :wink:

## Setup

### WordPress

-   Install and activate the [Degil plugin](https://gitlab.com/valiant.id/wp-plugins/degil) and its dependencies (composer should install them automatically).
-   Set permalink to custom structure, with a prefix, eg `/blog/%postname%/`.

### Frontend

-   Clone this repo.
-   Copy `.env.example` to `.env` and change the values accordingly.
-   Install dependencies; `pnpm install`.
-   To develop and/or build, first go to `packages/site` directory: `cd packages/site`, then:
    -   Develop: `pnpm dev` and then visit `http://localhost:3000`.
    -   Build : `pnpm build`.
    -   Run the build locally with docker: `docker run --rm -p 3001:80 -v $(pwd)/public:/usr/share/nginx/html nginx:alpine` and then visit `http://localhost:3001`.
    -   Deploy the `public/` directory :tada:
