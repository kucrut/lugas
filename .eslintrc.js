const path = require( 'path' );

module.exports = {
	root: true,
	extends: [
		'plugin:@wordpress/eslint-plugin/custom',
		'plugin:@wordpress/eslint-plugin/esnext',
		'plugin:@wordpress/eslint-plugin/jsdoc',
	],
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	overrides: [ { files: [ '*.html', '*.svelte' ], processor: 'svelte3/svelte3' } ],
	parser: '@babel/eslint-parser',
	parserOptions: {
		babelOptions: {
			// Credit: https://github.com/standard/vscode-standard/issues/100#issuecomment-799668282
			configFile: path.join( __dirname, 'babel.config.js' ),
		},
		ecmaVersion: 2019,
		sourceType: 'module',
		allowImportExportEverywhere: true,
	},
	plugins: [ '@babel', 'svelte3' ],
	settings: {
		'svelte3/ignore-styles'() {
			return true;
		},
	},
	rules: {
		'arrow-parens': [ 'error', 'as-needed' ],
		'camelcase': 'off',
		'no-multiple-empty-lines': [
			'error',
			{
				max: 1,
				maxBOF: 2,
				maxEOF: 1,
			},
		],
		'quote-props': [ 'error', 'consistent' ],
		// Plugins
		'jsdoc/valid-types': 'off',
	},
};
